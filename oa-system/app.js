const express = require("express");
const multer = require("multer");

const verifyToken = require("./utiles/verify_token");
const log = require("./utiles/log");
const roleAuth = require("./utiles/role_auth");
const routers = require("./routes");

const app = express();
const upload = multer({ dest: "./upload" });

// 生成超管信息
// const addAdmin = require("./sql/addAdmin");
// addAdmin();
// 注册中间件，解析前端发送的 数据类型为 application/json 的请求体

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static("./static"));
app.use(express.static("./upload"));
app.use(upload.any());
// cors配置
app.all("*", (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "get,post");
    res.header(
        "Access-Control-Allow-Headers",
        "Content-Type, Content-Length, Authorization, Accept, X-Requested-With, token"
    );
    next();
});
app.use(verifyToken);
app.use(roleAuth);
app.use(log);
app.use(routers);

app.listen(3000, () => {
    console.log("OA系统baseURL：http://localhost:3000");
});
