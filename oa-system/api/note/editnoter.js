const notes = require("../../sql/notes");
// 验证

module.exports = (req, res, next) => {
    let {
        tokenData,
        body: { title, content, token, noteid },
    } = req;
    if (tokenData.role !== 2) {
        // 空数组
        const noteMsg = {};
        if (title) noteMsg.title = title;
        if (content) noteMsg.content = content;
        noteMsg.sendTime = Date.now();
        // 修改
        // console.log("编辑",tokenData.accountId,noteid)
        notes.updateOne(
            { $and: [{ noteid }, { accountId: tokenData.accountId }] },
            noteMsg,
            (err, infoaa) => {
                if (err) return res.send({ code: 3, msg: "修改失败" });
                if (infoaa.modifiedCount >= 1) {
                    res.send({
                        code: 200,
                        msg: "修改成功",
                        noteMsg,
                    });
                } else {
                    res.send({
                        code: 2,
                        msg: "只能修改自己创建的公告",
                    });
                }
            }
        );
    } else {
        res.send({
            code: 4,
            msg: "权限不足",
        });
    }
};
