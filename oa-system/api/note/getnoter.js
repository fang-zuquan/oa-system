// 定义的数据类型
const notes = require("../../sql/notes");
// 验证token的插件
//搜索是通过标题查询，所有人可查询所有任务
// 只有管理员和经理可以 编辑和删除自己发布的 任务
// 员工只能查询任务不能修改也不能编辑
// 判断是否为登录状态
module.exports = (req, res) => {
    const { noteId, noteTitle } = req.body;
    const query = {};
    noteId && (query.noteid = noteId);
    noteTitle && (query.title = noteTitle);
    notes
        .find(query, { _id: 0, __v: 0 }, (err, data) => {
            res.send({
                code: 200,
                msg: "获取所有任务信息成功",
                data,
            });
        })
        .sort({ sendTime: -1 });
};
