const notes = require("../../sql/notes");
// 引入验证token
// 管理员 /经理 可 以添加公告信息
const uuid = require("uuid");
// const token = require("token")
const staff = require("../../sql/staffs");

module.exports = (req, res, next) => {
    let {
        tokenData,
        body: { title, content, token },
    } = req;
    // 验证登录状态
    if (tokenData.role !== 2) {
        let staffName = [{}];
        staff.find({ staffId: tokenData.staffId }, (err, data) => {
            staffName = data[0].staffName;
            const user = {
                noteid: "note-" + uuid.v1(),
                title,
                content,
                staffName,
                sendTime: Date.now(),
                accountId: tokenData.accountId,
            };
            notes.insertMany(user, (err, data) => {
                if (err) return res.send({ code: 2, msg: "添加失败" });
                res.send({
                    code: 200,
                    msg: "管理员任务添加成功",
                    data,
                });
            });
        });
    } else {
        res.send({
            code: 200,
            msg: "员工无权限修改任务，请联系管理员",
        });
    }
};
