const notes = require("../../sql/notes");

module.exports = (req, res) => {
    let {
        tokenData,
        body: { noteid },
    } = req;
    if (tokenData.role === 0) {
        // 只有 管理员 可以删除所有的公告，经理值可以删除自己创建的公告
        notes.deleteOne({ noteid }, (err, info) => {
            if (err) return res.send({ code: 200, msg: "出错了" + err });
            if (info.deletedCount === 0) {
                return res.send({ code: 3, msg: "不存在任务" });
            }
            res.send({ code: 200, msg: "管理员任务删除成功" });
        });
        // 如果登录的是经理的，只能删除自己添加的任务
    } else if (tokenData.role === 1) {
        // $and 或条件
        notes.deleteOne(
            { $and: [{ noteid }, { accountId: tokenData.accountId }] },
            (err, data) => {
                if (err) return res.send({ code: 2, msg: "出错了" + err });
                return res.send({ code: 2, msg: "任务删除成功" });
            }
        );
    } else {
        res.send({ code: 111, msg: "daskhdgk" });
    }
};
