const tasks = require("../../sql/tasks");
const uuid = require("uuid");
// 只有经理可以创建任务，经理role为1
module.exports = (req, res, next) => {
    let {
        tokenData: { role, staffId: sendId, staffName: sendName },
        body: { title, content, startTime, endTime, staffName: oriStaffName },
    } = req;
    if (role === 1) {
        const [staffId, staffName] = oriStaffName.split("_&_");
        const task = {
            tasksId: "tasks-" + uuid.v1(), //任务ID
            title, //任务 标题
            startTime, //开始时间
            endTime, //结束时间
            content, //任务详情
            staffId, //员工ID
            staffName,
            sendName,
            sendId,
            creatTime: Date.now(), //创建任务时间
            approveState: "待完成", //状态
            postscript: "",
        };
        console.log(task);
        tasks.insertMany(task, (err, data) => {
            if (err) return res.send({ code: 400, msg: "添加失败" });
            res.send({
                code: 200,
                msg: "任务创建成功",
                data,
            });
        });
    } else {
        res.send({
            code: 403,
            msg: "权限不够，请联系部门经理",
        });
    }
};
