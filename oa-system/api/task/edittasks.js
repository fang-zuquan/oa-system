const sectors = require("../../sql/sectors");
const tasks = require("../../sql/tasks");
// 操作任务的状态，完成或未完成
//
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: { tasksId, token, approveState, postscript },
    } = req;

    if (!(approveState || postscript))
        return res.send({ code: 200, msg: "请传入新信息" });
    const newDetail = {};
    if (approveState) newDetail.approveState = approveState;
    if (postscript) newDetail.postscript = postscript;
    tasks.updateOne({ tasksId }, newDetail, (err, info) => {
        console.log(err);
        if (err) return res.send({ code: 2, msg: "修改失败" });
        res.send({ code: 3, msg: "修改成功" });
    });
};
