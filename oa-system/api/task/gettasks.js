// 定义的数据类型
const tasks = require("../../sql/tasks");
const findDb = require("../../modules/find_db");
// 验证token的插件
// 员工role为2，根据员工staffid查询属于自己的任务
// 经理role为1，根据部门accountId查询自己部门的任务
// 判断是否为登录状态
module.exports = (req, res) => {
    let {
        tokenData: { staffId },
        query: { tasksId },
    } = req;
    let query = !!tasksId
        ? { tasksId }
        : { $or: [{ sendId: staffId }, { staffId }] };

    findDb(tasks, query, res).then((data) => {
        // console.log(data, query, req.tokenData);
        res.send({
            code: 200,
            msg: "获取任务信息成功",
            data,
        });
    });
};
