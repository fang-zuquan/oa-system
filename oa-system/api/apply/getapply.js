// 定义的数据类型
const apply = require("../../sql/apply");
// 验证token的插件
const jwt = require("jsonwebtoken");
const tasks = require("../../sql/tasks");

module.exports = (req, res) => {
    let {
        tokenData,
        query: { applyId },
    } = req;
    // 解析token，登录验证
    if (!applyId) {
        tasks.find(
            {
                $in: [
                    { staffId: tokenData.staffId },
                    { approve: tokenData.staffId },
                ],
            },
            (err, data) => {
                if (err) return res.send({ code: 200, msg: "查询失败" });
                res.send({
                    code: 3,
                    msg: "获取任务信息成功",
                    data,
                });
            }
        );
    } else {
        tasks.find({ tasksId }, (err, data) => {
            if (err) return res.send({ code: 200, msg: "查询失败" });
            res.send({
                code: 3,
                msg: "获取任务信息成功",
                data,
            });
        });
    }
};
