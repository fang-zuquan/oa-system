const applay = require("../../sql/apply");
const jwt = require("jsonwebtoken");
const uuid = require("uuid");
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: { applyId, approveState, approveRemark },
    } = req;
    // 验证登录状态
    applay.find({ applyId }, (err, data) => {
        if (err) return console.log("该申请不存在");
        if (data[0].approve !== tokenData.staffId)
            return res.send({ code: 2, msg: "你无权修改此申请" });
        applay.updateOne(
            { applyId },
            { approveState, approveRemark },
            (err, info) => {
                if (err) return res.send({ code: 3, msg: "修改失败" });
                res.send({
                    code: 200,
                    msg: "修改成功",
                });
            }
        );
    });
};
