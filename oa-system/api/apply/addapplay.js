const applay = require("../../sql/apply");
const jwt = require("jsonwebtoken");
const uuid = require("uuid");
const staffs = require("../../sql/staffs");
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: {
            title,
            content,
            applyType,
            approve,
            startTime,
            endTime,
            singleTime,
            money,
            payEndTime,
            job,
        },
    } = req;
    // 验证登录状态
    let sendPeople = "";
    let approvePeople = "";
    const s = staffs.find({ staffId: tokenData.staffId });
    const a = staffs.find({ staffId: approve });
    Promise.all([s, a]).then((all) => {
        sendPeople = all[0][0].staffName;
        approvePeople = all[1][0].staffName;
        const allHave = {
            applyId: "apply-" + uuid.v1(),
            title,
            content,
            applyType,
            approve,
            sendPeople,
            approvePeople,
            approveState: 0,
            staffId: tokenData.staffId,
        };
        const twoTime = { startTime, endTime };
        const fund = { money, payEndTime };
        let finalyData = null;
        if (applyType === 0 || applyType === 1 || applyType === 3) {
            if (
                !(
                    title &&
                    content &&
                    (applyType === 0 || applyType === 1 || applyType === 3) &&
                    approve &&
                    startTime &&
                    endTime
                )
            )
                return res.send({
                    code: 3,
                    msg: "请输入所有信息,表单中所有的数据都需要填写",
                });
            // 请假 加班 出差
            finalyData = { ...allHave, ...twoTime };
        } else if (applyType === 2 || applyType === 4) {
            if (!(title && content && applyType && approve && singleTime))
                return res.send({
                    code: 3,
                    msg: "请输入所有信息,表单中所有的数据都需要填写",
                });
            // 补卡 离职
            finalyData = { ...allHave, singleTime };
        } else if (applyType === 5) {
            // 招聘
            if (!(title && content && applyType && approve && job))
                return res.send({
                    code: 3,
                    msg: "请输入所有信息,表单中所有的数据都需要填写",
                });
            finalyData = {
                ...allHave,
                job,
                sectorId: tokenData.sectorId,
            };
        } else if (applyType === 6) {
            if (
                !(
                    title &&
                    content &&
                    applyType &&
                    approve &&
                    money &&
                    payEndTime
                )
            )
                return res.send({
                    code: 3,
                    msg: "请输入所有信息,表单中所有的数据都需要填写",
                });
            finalyData = { ...allHave, ...fund };
        }
        applay.insertMany(finalyData, (err, info) => {
            if (err) return res.send({ code: 2, msg: "新增失败" });
            res.send({ code: 200, msg: "申请已发送，待审批" });
        });
    });
    ``;
};
