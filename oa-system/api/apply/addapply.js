// 定义的数据类型
const apply = require("../../sql/apply");
// 验证token的插件
const uuid = require("uuid");

module.exports = (req, res) => {
    let {
        tokenData,
        body: {
            title,
            content,
            job, // 申请岗位
            applyType,
            money,
            endTimeFund,
            payEndTime,
            singleTime,
            endTime,
            startTime,
            sendPeople,
        },
    } = req;
    // 解析token，登录验证
    const obj = {
        applyId: "apply-" + uuid.v1(),
        staffId: tokenData.staffId, // 发起人的员工id
        title, // 标题
        content, // 原因
        applyType: applyType - 0, //申请类型 0请假 1加班 2补卡 3出差 4离职 5招聘 6经费
    };
    if (obj.applyType === 6) {
        money && (obj.money = money);
        endTimeFund && (obj.endTimeFund = endTimeFund);
    } else if (obj.applyType === 6) {
        obj.sectorId = tokenData.sectorId;
        job && (obj.job = job);
    } else if (1) {
        console.log("未完成，请继续");
    }
    apply.insertMany({}, (err, data) => {
        if (err) return res.send({ code: 200, msg: "插入失败" });
        res.send({ code: 200, msg: "插入成功" });
    });
};
