//根据员工id 获取员工姓名
// {
//     id:'xxx'
// }
const staffs = require("../sql/staffs");

module.exports = (req, res) => {
    let { id } = req.query;
    staffs.find({ staffId: id }).then((data) => {
        if (data.length < 1) {
            res.send({
                code: "0",
                msg: "该员工编号不存在",
            });
        } else {
            res.send({
                code: "1",
                msg: data[0].staffName,
            });
        }
    });
};
