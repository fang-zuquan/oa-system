const clockingins = require("../sql/clockingin");
// const staffs = require('../sql/staffs');
const jwt = require("jsonwebtoken");
module.exports = (req, res) => {
    //----------------------
    clockingins.find({}, { _id: 0, __v: 0 }, (err, initData) => {
        const data = []; //处理后的数据
        // initData.forEach(
        //     item => {
        //         const id = item.id
        //         //==========
        //         item.checkData.forEach(i => {
        //             // i.attStatusShow = i.attStatus+'状态'
        //             switch (i.attStatus) {
        //                 case '0': i.attStatusShow = '正常'; break
        //                 case '1': i.attStatusShow = '迟到'; break
        //                 case '2': i.attStatusShow = '早退'; break
        //                 case '3': i.attStatusShow = '缺卡'; break
        //                 case '4': i.attStatusShow = '加班'; break
        //                 case '5': i.attStatusShow = '请假'; break
        //                 case '6': i.attStatusShow = '出差'; break
        //                 default: i.attStatusShow = '？'
        //             }
        //             i.id = item.id;
        //             data.push(i);
        //         });
        //         //==========
        //     }
        // )
        initData.forEach((item) => {
            const id = item.id;
            const name = item.name;
            //==========
            item.checkData.forEach((i) => {
                // i.attStatusShow = i.attStatus+'状态'
                switch (i.attStatus) {
                    case "0":
                        i.attStatusShow = "正常";
                        break;
                    case "1":
                        i.attStatusShow = "迟到";
                        break;
                    case "2":
                        i.attStatusShow = "早退";
                        break;
                    case "3":
                        i.attStatusShow = "缺卡";
                        break;
                    case "4":
                        i.attStatusShow = "加班";
                        break;
                    case "5":
                        i.attStatusShow = "请假";
                        break;
                    case "6":
                        i.attStatusShow = "出差";
                        break;
                    default:
                        i.attStatusShow = "？";
                }
                i.id = item.id;
                i.name = item.name;
                data.push(i);
            });
            //==========
        });
        res.send({
            code: 3,
            msg: "获取考勤信息成功",
            data,
        });
    });
    //----------------------
};
