const contracts = require("../../sql/contract");
// 如果传入了contractId，表示只获取当前合同信息
// XXXXXXXX如果没有传入contractId，表示获取所有账户信息XXXXX
module.exports = (req, res) => {
    const { token } = req.query;
    // 解析token，登录验证
    contracts.find({}, { _id: 0, __v: 0 }, (err, data) => {
        res.send({
            code: 3,
            msg: "获取全部合同成功",
            data,
        });
    });

    // else if (tokenData.role === 0) {
    //   // 管理员权限
    //   if (!contractId) {
    //     // 没有发contractsId
    //     contracts.find({}, { _id: 0, __v: 0 }, (err, data) => {
    //       res.send({
    //         code: 200,
    //         msg: "获取所有账户信息成功",
    //         data,
    //       });
    //     });
    //   } else {
    //     // 发contractsId
    //     contracts.find({ contractId }, { _id: 0, __v: 0 }, (err, data) => {
    //       res.send({
    //         code: 3,
    //         msg: "获取单条账户信息成功",
    //         data,
    //       });
    //     });
    //   }
    // } else {
    //   // 经理和员工
    //   // console.log(tokenData);
    //   // accounts.find({ accountId: tokenData.accountId }, { _id: 0, __v: 0 }, (err, data) => {
    //   //   res.send({
    //   //     code: 200,
    //   //     msg: "获取账户信息成功",
    //   //     data,
    //   //   });
    //   // });
    // }
};
