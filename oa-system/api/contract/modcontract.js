// const contract = require("../sql/contract");
// const jwt = require("jsonwebtoken");
// const uuid = require('uuid');
// const logs = require('../sql/logs');
// const getClientIp = require('./getclientip');
// module.exports = (req, res) => {
//   const { contractId, contractName, id, employeeName, contractType,
//     effectiveDate, endDate, addedDate, updatedDate, notes, token } = req.body;
// console.log('----', token)
//   jwt.verify(token, "有志者事竟成", (tokenErr, tokenData) => {
//     if (tokenErr) {
//       res.send({
//         code: 0,
//         msg: "登录失效",
//       });
//     } else {
//       contract.updateOne({ contractId }, userMsg, (err, info) => {
//         // console.log(err, info, tokenData.accountId);
//         if (info.modifiedCount >= 1) {
//           res.send({
//             code: 200,
//             msg: "修改成功",
//             userMsg,
//           });
//           //添加日志
//           logs.insertMany({
//             id:"log-" + uuid.v1(),
//             username:tokenData.username,
//             accountId:tokenData.accountId,
//             operate:"修改人事合同",
//             operateTime: Date.now(),
//             ipAddress:getClientIp.getClientIp(req)
//           })
//         } else if (info.matchedCount >= 1) {
//           res.send({
//             code: 2,
//             msg: "信息相同，没有修改",
//           });
//         } else {
//           res.send({
//             code: 3,
//             msg: "账户id错误，修改失败",
//           });
//       contract.updateOne({ contractId }, {
//         $set: {
//           contractName, id, employeeName, contractType,
//           effectiveDate, endDate, addedDate, updatedDate, notes
//         }
//       }, (err) => {
//         res.send({
//           code: 200,
//           msg: "修改成功",
//         });
//       })
//     }
//   });
// };
const contract = require("../../sql/contract");
const jwt = require("jsonwebtoken");

module.exports = (req, res) => {
    const {
        contractId,
        contractName,
        id,
        employeeName,
        contractType,
        effectiveDate,
        endDate,
        addedDate,
        updatedDate,
        notes,
        token,
    } = req.body;
    console.log("----", token);
    contract.updateOne(
        { contractId },
        {
            $set: {
                contractName,
                id,
                employeeName,
                contractType,
                effectiveDate,
                endDate,
                addedDate,
                updatedDate,
                notes,
            },
        },
        (err) => {
            res.send({
                code: 200,
                msg: "修改成功",
            });
        }
    );
};
