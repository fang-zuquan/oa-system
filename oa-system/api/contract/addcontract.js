const contract = require("../../sql/contract");

module.exports = (req, res) => {
    let {
        contractId,
        contractName,
        id,
        employeeName,
        contractType,
        effectiveDate,
        endDate,
        addedDate,
        updatedDate,
        notes,
        token,
    } = req.body;
    contract.find({ contractId }).then((data) => {
        if (data.length < 1) {
            //===========可以添加======================
            contract.insertMany({
                contractId,
                contractName,
                id,
                employeeName,
                contractType,
                effectiveDate,
                endDate,
                addedDate,
                updatedDate,
                notes,
            });
            res.send({
                msg: "添加成功",
            });
            //===========可以添加======================
            //添加日志
        } else {
            res.send({
                msg: "该合同编号已被使用",
                contractId,
            });
        }
    });
};
