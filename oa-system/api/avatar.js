const accounts = require("../sql/accounts");
const jwt = require("jsonwebtoken");
const path = require("path");
const fs = require("fs");
module.exports = (req, res) => {
    const { tokenData } = req;
    const file = req.files[0];
    let serverPath = "";
    if (file !== "") {
        const oldName = path.join(__dirname, "..", file.path);
        const newName = oldName + "-" + file.originalname;
        fs.renameSync(oldName, newName);
        serverPath = "http://localhost:3000" + newName.split("upload")[1];
        accounts.updateOne(
            { accountId: tokenData.accountId },
            { avatar: serverPath },
            (a, b, c, d) => {
                res.send({
                    code: 200,
                    msg: "头像上传成功",
                    url: serverPath,
                });
            }
        );
    }
};
