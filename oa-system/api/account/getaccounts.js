const accounts = require("../../sql/accounts");
const findDb = require("../../modules/find_db");
// 如果传入了accountId，表示只获取当前账户信息
// 如果没有传入accountId，表示获取所有账户信息
module.exports = (req, res) => {
    const {
        query: { accountId },
        tokenData: { role, accountId: tokenAccountId },
    } = req;
    let query =
        role === 0
            ? !!accountId
                ? { accountId }
                : {}
            : { accountId: tokenAccountId };

    findDb(accounts, query, res).then((data) => {
        res.send({
            code: 200,
            msg: "获取成功",
            data,
        });
    });
};
