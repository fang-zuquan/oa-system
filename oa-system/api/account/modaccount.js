const accounts = require("../../sql/accounts");
const md5 = require("md5");
// 管理员可以修改所有用户的所有信息
// token，power，要修改用户的uId，要修改的信息
// 非管理员只能修改个人有限的信息：密码，头像（其他接口）
module.exports = (req, res, next) => {
    const {
        tokenData,
        body: { username, oldPassword, tel, newPassword, accountId },
    } = req;
    const userMsg = {};
    const find = {};
    if (tokenData.role === 0) {
        if (username) userMsg.username = username;
        if (newPassword) userMsg.password = md5(newPassword);
        if (tel) userMsg.tel = tel - 0;
        accountId
            ? (find.accountId = accountId)
            : (find.accountId = tokenData.accountId);
        console.log(find, userMsg);
        accounts.updateOne(find, userMsg, (err, info) => {
            console.log(err, info, tokenData.accountId);
            if (info.modifiedCount >= 1) {
                res.send({
                    code: 200,
                    msg: "修改成功",
                    userMsg,
                });
            } else if (info.matchedCount >= 1) {
                res.send({
                    code: 2,
                    msg: "信息相同，没有修改",
                });
            } else {
                res.send({
                    code: 3,
                    msg: "账户id错误，修改失败",
                });
            }
        });
    } else {
        const newInfo = {};
        if (username) newInfo.username = username;
        if (newPassword) newInfo.password = md5(newPassword);
        if (tel) newInfo.tel = tel - 0;
        accounts.find({ accountId: tokenData.accountId }).then((accRes) => {
            if (accRes.length === 0)
                return res.send({ code: 4, msg: "账号不存在" });
            if (accRes[0].password !== md5(oldPassword))
                return res.send({
                    code: 5,
                    msg: "老密码错误，若忘记老密码，请联系管理员处理",
                });
            accounts.updateOne(
                { accountId: tokenData.accountId },
                newInfo,
                (err, info) => {
                    if (info.modifiedCount >= 1) {
                        res.send({
                            code: 200,
                            msg: "修改成功",
                            userMsg,
                        });
                    } else if (info.matchedCount >= 1) {
                        res.send({
                            code: 2,
                            msg: "信息相同，没有修改",
                        });
                    } else {
                        res.send({
                            code: 3,
                            msg: "账户id错误，修改失败",
                        });
                    }
                }
            );
        });
    }
};
