const accounts = require("../../sql/accounts");
// 管理员可以修改所有用户的所有信息
// token，power，要修改用户的uId，要修改的信息
// 非管理员只能修改个人有限的信息：密码，头像（其他接口）
module.exports = (req, res, next) => {
    const { token, staffId, sectorId, role, state, accountId } = req.body;
    const userMsg = {};
    if (staffId) userMsg.staffId = staffId;
    if (sectorId) userMsg.sectorId = sectorId;
    if (role) userMsg.role = role - 0;
    if (state >= 0) userMsg.state = state - 0;
    accounts.updateOne({ accountId: accountId }, userMsg, (err, info) => {
        if (info.modifiedCount >= 1) {
            res.send({
                code: 200,
                msg: "修改成功",
                userMsg,
            });
        } else if (info.matchedCount >= 1) {
            res.send({
                code: 3,
                msg: "信息相同，没有修改",
            });
        } else {
            res.send({
                code: 4,
                msg: "账户id错误，修改失败",
            });
        }
    });
};
