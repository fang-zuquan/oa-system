const accounts = require("../../sql/accounts");
const uuid = require("uuid");
const md5 = require("md5");
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: {
            username,
            staffId,
            sectorId,
            password = "123456",
            state = 0,
            role = 2,
            token,
        },
    } = req;

    // {$and:[ {expression1},{expression2}, ... ,{expressionN} ]}
    accounts.find({ username }, (err, data) => {
        if (data.length < 1) {
            accounts.find({ staffId }, (err, data) => {
                if (data.length < 1) {
                    const userMsg = {
                        accountId: "account-" + uuid.v1(),
                        staffId,
                        sectorId,
                        username,
                        password: md5(password),
                        role,
                        state: state - 0,
                        avatar: "",
                    };
                    accounts.insertMany(userMsg, (err) => {
                        res.send({
                            code: 2,
                            msg: "账号信息添加成功",
                        });
                    });
                } else {
                    res.send({
                        code: 4,
                        msg: "该员工已有账号，账号为：" + data[0].username,
                    });
                }
            });
        } else {
            res.send({
                code: 3,
                msg: "该账号已存在",
            });
        }
    });
};
