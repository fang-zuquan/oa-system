const logs = require("../sql/logs");
const findDb = require("../modules/find_db");
module.exports = (req, res) => {
    const { startDate, endDate } = req.body;
    let query =
        startDate && endDate
            ? { operateTime: { $lte: endDate, $gte: startDate } }
            : {};

    findDb(logs, query, req, {
        sort: true,
        sortReq: "operateTime",
    }).then((data) => {
        res.send({
            code: 200,
            msg: "获取操作日志成功",
            data,
        });
    });
};
// 查所有数据的所有字段
// 语法：集合对象.find(回调函数)
// 回调函数：查询完成后要执行的功能
// 参数1：报错信息，null为成功，对象为失败
// 参数2：数据，数组形式
// user.find((err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 查所有数据的指定字段
// 语法：集合对象.find(参数1,参数2,回调函数)
// 参数1：条件，空对象表示没有条件
// 参数2：约束字段，对象数据。key为要约束的字段，0为隐藏，1为显示
// 回调函数：查询完成后要执行的功能
// user.find({}, {_id:0, __v:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 查指定数据
// 语法：集合对象.find(参数1, 参数2, 回调函数)
// 参数1：对象，表示查询条件，键值对
// user.find({username:"张三"}, {username:1, password:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 模糊查询
// 语法：集合对象.find(参数1, 参数2, 回调函数)
// 参数1：对象，表示查询条件，键值对，值为正则
// user.find({username: /^张/}, {username:1, password:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// user.find({username: /张/}, {username:1, password:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 排序查询
// 语法：集合对象.find(参数1, 参数2, 回调函数).sort(排序条件)
// 排序条件：对象，可以为要排序的字段，值为1表示升序，-1表示降序
// user.find({}, {username:1, age:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// }).sort({age:-1})
// 区间查
// 语法：集合对象.find(参数1, 参数2, 回调函数)
// 参数1：对象，key要区间查的字段，值又是一个对象，key为$lt表示小于，$lte表示小于等于，$gt表示大于，$gte表示大于等于
// user.find({ age: { $lt:30, $gt:20 } }, {username:1, age:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// user.find({ age: { $lte:30, $gte:20 } }, {username:1, age:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 且查询
// 语法：集合对象.find(参数1, 参数2, 回调函数)
// 参数1：对象，直接存在多个键值对即可
// user.find({ username: /张/, age:{$gt:23} }, {username:1, age:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 或查询
// 语法：集合对象.find(参数1, 参数2, 回调函数)
// 参数1：对象，key为$or，值为数组，数组内又是对象，每个对象内放一个条件
// user.find({ $or:[{username:"张三"},{username:"李四"}] }, {username:1, age:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 分页查
// 语法：集合对象.find(参数1, 参数2, 回调函数).limit(数量).skip(起始索引)
// user.find({ }, {username:1, age:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// }).limit(2).skip(2);
// 单页条数：
// let num = 2;
// 页码：
// let index = 0;
// user.find({ }, {username:1, age:1, _id:0}, (err, data)=>{
//     console.log(err)
//     console.log(data)
// }).limit(num).skip(index * num);
// 查分类
// 语法：集合对象.distinct(参数1, 回调函数)
// 参数1：字符，要分类的字段名
// user.distinct("isVIP", (err, data)=>{
//     console.log(err)
//     console.log(data)
// })
// 查询总条数
// user.countDocuments((err,data)=>{
//     console.log(err)
//     console.log(data)
// })
