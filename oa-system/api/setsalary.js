const salary = require("../sql/salaryset");
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: {
            sendDate,
            lateFine,
            earlyFine,
            absenceFine,
            lackFine,
            overWork,
            accountId,
            addTime,
        },
    } = req;
    salary.find({ accountId }, (err, data) => {
        if (err)
            return res.send({
                code: 4,
                msg: "没有用户accountId，数据请求失败",
            });
        if (data.length > 0) {
            const data = {
                sendDate,
                lateFine,
                earlyFine,
                absenceFine,
                lackFine,
                overWork,
                addTime: Date.now(),
            };
            // const data = {};
            // if (sendDate) data.sendDate = sendDate;
            // if (lateFine) data.lateFine = lateFine;
            salary.updateOne({ accountId }, data, (err, info) => {
                if (info.modifiedCount >= 1) {
                    res.send({
                        code: 2,
                        msg: "更新薪酬设置成功",
                        data,
                    });
                } else if (info.matchedCount >= 1) {
                    res.send({
                        code: 5,
                        msg: "信息相同，没有修改",
                    });
                } else {
                    res.send({
                        code: 6,
                        msg: "错误，修改失败",
                    });
                }
            });
        } else {
            const salaryData = {
                sendDate,
                lateFine,
                earlyFine,
                absenceFine,
                lackFine,
                overWork,
                addTime: Date.now(),
            };
            salary.insertMany(salaryData, (err, data) => {
                if (err)
                    return res.send({
                        code: 7,
                        msg: "设置失败，请重试",
                    });
                res.send({
                    code: 3,
                    msg: "添加薪酬设置成功",
                    data,
                });
            });
        }
    });
};
