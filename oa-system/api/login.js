const accounts = require("../sql/accounts");
const md5 = require("md5");
const jwt = require("jsonwebtoken");
const staffs = require("../sql/staffs");
const findDb = require("../modules/find_db");

function handleLogin(obj) {
    const {
        password,
        pw,
        state,
        staffId,
        username,
        sectorId,
        accountId,
        role,
        staffName,
        avatar,
    } = obj;

    const base = { role, staffId, username, accountId, avatar };
    const inputPw = md5(pw);
    if (!staffId) return { code: 400, msg: "用户名不存在" };
    if (password !== inputPw) return { code: 400, msg: "密码不对" };
    if (state !== 0)
        return {
            code: 400,
            msg: "登陆失败，当前账号已被禁用，如需使用请联系管理员重启该账号",
        };

    const token = jwt.sign({ staffName, sectorId, ...base }, "有志者事竟成", {
        expiresIn: 60 * 60 * 24 * 3,
    });
    return {
        code: 200,
        msg: "账号登录",
        data: {
            ...base,
            token,
        },
    };
}
module.exports = async (req, res) => {
    // 解析前端发送的数据
    const un = req.query.username;
    const pw = req.query.password;
    // 根据用户名读取数据库
    const [account] = await findDb(accounts, { username: un }, res);

    const [{ staffName } = {}] = await findDb(
        staffs,
        { staffId: account?.staffId },
        res
    );

    let body = handleLogin({ ...(account?._doc || {}), pw, staffName });
    res.send(body);
};
