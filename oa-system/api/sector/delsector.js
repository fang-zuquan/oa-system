const sectors = require("../../sql/sectors");
// 只有管理员能删除部门
// 非管理员没有权限删除
module.exports = (req, res, next) => {
    const { token, sectorId } = req.body;
    sectors.deleteOne(
        {
            sectorId,
        },
        (err) => {
            res.send({
                code: 200,
                msg: "删除成功",
            });
        }
    );
};
