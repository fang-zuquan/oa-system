const sectors = require("../../sql/sectors");

module.exports = (req, res) => {
    const { token } = req.query;
    // 解析token，登录验证
    // 没有发  id
    sectors.find({}, { _id: 0, __v: 0 }, (err, data) => {
        res.send({
            code: 200,
            msg: "获取所有部门信息成功",
            data,
        });
    });
};
