const sectors = require("../../sql/sectors");

module.exports = (req, res, next) => {
    let { sectorName, token, sectorId, manage } = req.body;

    const sectorMsg = {};
    if (sectorName) sectorMsg.sectorName = sectorName;
    if (manage) sectorMsg.allStaff = manage;
    sectors.updateOne({ sectorId }, sectorMsg, (err, info) => {
        if (info.modifiedCount >= 1) {
            res.send({
                code: 200,
                msg: "修改成功",
                sectorMsg,
            });
        } else if (info.matchedCount >= 1) {
            res.send({
                code: 3,
                msg: "部门名称相同，请修改成其它名称",
            });
        } else {
            res.send({
                code: 4,
                msg: "错误，修改失败",
            });
        }
    });
};
