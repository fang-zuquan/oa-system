const sectors = require("../../sql/sectors");
const uuid = require("uuid");

module.exports = (req, res, next) => {
    let {
        tokenData,
        body: { sectorName, allStaff, staffNum, addTime, manage, token },
    } = req;
    sectors.find({ sectorName }, (err, data) => {
        if (err) return res.send({ code: 3, msg: "查询失败" });
        if (data.length > 0)
            return res.send({
                code: 4,
                msg: "该部门已经存在，请添加其它部门",
            });
        const sector = {
            sectorId: "sector-" + uuid.v1(),
            sectorName,
            allStaff,
            staffNum,
            addTime: Date.now(),
            manage,
        };
        sectors.insertMany(sector, (err, data) => {
            if (err) return res.send({ code: 3, msg: "添加失败" });
            res.send({
                code: 2,
                msg: "部门添加成功",
                data,
            });
            
        });
    });
};
