const staffs = require("../../sql/staffs");
const accounts = require("../../sql/accounts");
const md5 = require("md5");
const sectors = require("../../sql/sectors");
// cardId 必传
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: { token, staffId },
    } = req;

    // 管理员
    // 传 staffId 的修改
    staffs.deleteOne({ staffId }, (err, data) => {
        if (err) return res.send({ code: 200, msg: "出错了" + err });
        if (data.deletedCount === 1) {
            sectors.find({ allStaff: staffId }, (err, data) => {
                if (data.length <= 0) return;
                let index = data[0].allStaff
                    ? data[0].allStaff.indexOf(staffId)
                    : -1;
                let newData =
                    index === -1
                        ? data[0].allStaff
                        : data[0].allStaff.splice(index, 1);
                sectors.updateOne(
                    { sectorId: data[0].sectorId },
                    { allStaff: newData },
                    (err, info) => {
                        console.log("删除员工时，更新部门信息");
                        console.log("err:", err);
                        console.log("info:", info);
                        console.log("删除员工时，更新部门信息结束");
                    }
                );
            });
            accounts.updateOne(
                { staffId },
                {
                    staffId: "",
                    sectorId: "",
                    password: md5(123456),
                    state: 1,
                    avatar: "",
                    tel: "",
                    role: 2,
                },
                (err, info) => {
                    //！ 到这了
                    if (!err) {
                        sectors.find({ allStaff: [staffId] }, (err, data) => {
                            console.log(err, data);
                            if (!err)
                                return res.send({
                                    code: 3,
                                    msg: "删除成功",
                                });
                        });
                    }
                }
            );
        } else {
            console.log(data);
        }
    });
};
