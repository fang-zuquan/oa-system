const staffs = require("../../sql/staffs");
const uuid = require("uuid");
const sectors = require("../../sql/sectors");
// cardId 必传
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: {
            sectorId,
            staffName,
            sex,
            address,
            nation,
            mianmao,
            tel,
            cardId = 0,
            school,
            graDate,
            major,
            eduBg,
            degree,
            job,
            remarks,
            salary,
            staffType,
            token,
        },
    } = req;

    staffs.find({ cardId }, (err, data) => {
        if (err) return res.send({ code: 3, msg: "查询失败" });
        if (data.length > 0)
            return res.send({
                code: 4,
                msg: "该员工已经存在，请检查身份证号码是否正确",
            });
        const staffId = "staff-" + uuid.v1();
        const user = {
            staffId,
            sectorId,
            staffName,
            sex,
            address,
            nation,
            mianmao,
            tel,
            cardId,
            school,
            graDate,
            major,
            eduBg,
            degree,
            job,
            remarks,
            salary,
            staffType,
            workStatus: 0,
            joinTime: Date.now(),
            leaveTime: -1,
        };
        staffs.insertMany(user, (err, data) => {
            if (err) return res.send({ code: 3, msg: "添加失败" });
            sectors.find({ sectorId }, (err, data) => {
                const newSta = data[0].allStaff.push(staffId);
                sectors.updateOne(
                    { sectorId },
                    { allStaff: newSta },
                    (err, info) => {
                        console.log(
                            "添加员工时，更新部门下的数据时的 err",
                            err
                        );
                        console.log(
                            "添加员工时，更新部门下的数据时的 info",
                            info
                        );
                    }
                );
            });
            res.send({
                code: 2,
                msg: "员工信息添加成功",
                data,
            });
        });
    });
};
