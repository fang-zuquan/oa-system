const staffs = require("../../sql/staffs");
const jwt = require("jsonwebtoken");
const token_verify = require("../../utiles/verify_token");
// 如果传入了staffId，表示只获取当前员工信息
// 如果没有传入staffId，表示获取所有员工信息
// {
//   sectorId: "",
//   staffType: null,
//   workStatus: null,
//   staffName: "",
//   token
// }
module.exports = (req, res) => {
    const {
        tokenData,
        query: { staffId, sectorId, staffType, workStatus, staffName } = {},
    } = req;
    // 解析token，登录验证
    console.log(tokenData);
    if (tokenData.role === 0) {
        console.log("到这里");
        // 管理员权限
        if (!staffId) {
            const searchFactor = [];
            if (!(sectorId || staffType || workStatus || staffName)) {
                searchFactor.push({});
            } else {
                sectorId && searchFactor.push({ sectorId });
                staffType && searchFactor.push({ staffType: staffType - 0 });
                workStatus && searchFactor.push({ workStatus: workStatus - 0 });
                staffName && searchFactor.push({ staffName });
            }
            staffs.find(
                { $and: searchFactor },
                { _id: 0, __v: 0 },
                (err, data) => {
                    res.send({
                        code: 200,
                        msg: "获取员工信息成功",
                        data,
                    });
                }
            );
        } else {
            // 发staffId
            staffs.find({ staffId }, { _id: 0, __v: 0 }, (err, data) => {
                res.send({
                    code: 3,
                    msg: "获取单条员工信息成功",
                    data,
                });
            });
        }
    } else if (tokenData.role === 1) {
        console.log("经理");
        // 经理
        if (!staffId) {
            // 没有发staffId
            staffs.find(
                { sectorId: tokenData.sectorId },
                { _id: 0, __v: 0 },
                (err, data) => {
                    res.send({
                        code: 200,
                        msg: "获取本部门所有员工信息成功",
                        data,
                    });
                }
            );
        } else {
            // 发staffId
            staffs.find(
                { $and: [{ sectorId: tokenData.sectorId }, { staffId }] },
                { _id: 0, __v: 0 },
                (err, data) => {
                    res.send({
                        code: 3,
                        msg: "获取单条员工信息成功",
                        data,
                    });
                }
            );
        }
    } else {
        console.log("员工");
        // 员工
        // console.log(tokenData);
        staffs.find(
            { staffId: tokenData.staffId },
            { _id: 0, __v: 0 },
            (err, data) => {
                res.send({
                    code: 200,
                    msg: "获取员工信息成功",
                    data,
                });
            }
        );
    }
};
