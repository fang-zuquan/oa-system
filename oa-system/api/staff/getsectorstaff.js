const staffs = require("../../sql/staffs");

module.exports = (req, res) => {
    const { sectorId } = req.body;
    if (sectorId) {
        staffs.find({ sectorId }, { _id: 0, __v: 0 }, (err, data) => {
            res.send({
                code: 200,
                msg: "获取本部门所有员工信息成功",
                data,
            });
        });
    } else {
        res.send({
            code: 2,
            msg: "请选择部门",
        });
    }
};
