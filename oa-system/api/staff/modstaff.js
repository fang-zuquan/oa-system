const staffs = require("../../sql/staffs");
// cardId 必传
module.exports = (req, res, next) => {
    let {
        tokenData,
        body: {
            sectorId,
            staffName,
            sex,
            address,
            nation,
            mianmao,
            tel,
            cardId = 0,
            school,
            graDate,
            major,
            eduBg,
            degree,
            job,
            remarks,
            salary,
            staffType,
            token,
            workStatus,
            staffId,
        },
    } = req;
    // console.log(req.body, workStatus, "salary:" + salary + ".");
    // staffId
    const user = { sectorId, cardId, staffName, salary, staffType, workStatus };
    const user2 = {
        sex,
        address,
        nation,
        mianmao,
        tel,
        school,
        graDate,
        major,
        eduBg,
        degree,
        job,
        remarks,
    };
    if (tokenData.role === 0) {
        // 管理员
        // 传 staffId 的修改
        if (req.body.staffId) {
            staffs.updateMany({ staffId }, user, (err, info) => {
                // console.log(err, info);
                if (err) return req.send({ code: 200, msg: "修改失败" });
                res.send({ code: 200, msg: "管理员修改指定个人功" });
            });
        } else {
            // 不传 staffId 的修改
            staffs.updateMany(
                { staffId: tokenData },
                { ...user, ...user2 },
                (err, info) => {
                    // console.log(err, info);
                    if (err) return req.send({ code: 200, msg: "修改失败" });
                    res.send({ code: 200, msg: "管理员修改管理员信息" });
                }
            );
        }
    } else {
        staffs.updateMany(
            { staffId: tokenData.staffId },
            user2,
            (err, info) => {
                // console.log(err, info);
                if (err) return req.send({ code: 200, msg: "修改失败" });
                res.send({ code: 200, msg: "修改个人" });
            }
        );
    }
};
