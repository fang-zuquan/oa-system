# 一、环境
1. 后端：node
   - 建议 node 版本 14 以上
2. 数据库：mongoDB
   - 建议 mongoDB 版本：5.0.12
# 二、数据库文件
- 数据库主文件：/sql/db.js
  - 可通过 dbAddress 修改 mongoDB 地址
  - 可通过 dbName 修改数据库名
# 三、启动
1. 命令行工具进入根目录
2. 下载依赖：`npm i`
3. 启动项目：`npm start`
4. 首页：`http://localhost:3000/index.html`
5. 接口 baseUrl：`http://localhost:3000`
