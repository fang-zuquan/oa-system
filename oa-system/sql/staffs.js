const mongoose = require("./db");
const staffSchema = new mongoose.Schema({
  staffId: { type: String }, // 员工编号 staff-111111
  sectorId: { type: String }, // 部门id
  staffName: { type: String }, // 姓名
  sex: { type: Number }, // 性别 男0 女1
  address: { type: String }, // 地址
  nation: { type: String }, // 民族
  mianmao: { type: Number }, // 政治面貌 群众0 党员1 预备党员2 共青团员3 民主党派4
  tel: { type: String }, // 手机号
  cardId: { type: String }, // 身份证号
  school: { type: String }, // 毕业学校
  graDate: { type: Number }, // 毕业日期
  major: { type: String }, // 专业
  eduBg: { type: Number }, // 学历 中专0 大专1 本科2 研究生3
  degree: { type: Number }, // 学位 无0 学士1 硕士2 博士3
  job: { type: String }, // 职位
  remarks: { type: String }, // 备注
  salary: { type: Number }, // 薪资
  staffType: { type: Number }, // 员工类型 "正式员工0、合同工1、临时工2"
  workStatus: { type: Number }, // 在职状态 "在职0、离职1、退休2"
  joinTime: { type: Number }, // 入职时间
  leaveTime: { type: Number }, // 离职时间
});
module.exports = mongoose.model("staffs", staffSchema);
//* token 的数据
//* staffId: data[0].staffId,
//* username: data[0].username,
//* sectorId: data[0].sectorId,
//* accountId: data[0].accountId,
//* role: data[0].role,