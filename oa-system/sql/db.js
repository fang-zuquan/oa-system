const mongoose = require("mongoose");
// mongoDB地址
const dbAddress = "mongodb://127.0.0.1:27017/";
// 数据库名
const dbName = "oasystem";
// mongoose.connect(dbAddress + dbName);
mongoose.connect(
    "mongodb://user:user987826fzq@fangzuquan.tk:4056/app?authSource=admin"
);
mongoose.connection.on("connected", () => {});
mongoose.connection.on("error", () => {
    console.log("数据库连接失败");
});
mongoose.connection.on("disconnected", () => {
    console.log("数据库连接断开");
});
module.exports = mongoose;
