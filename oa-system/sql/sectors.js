const mongoose = require("./db");
const sectorSchema = new mongoose.Schema({
  sectorId: { type: String }, // 部门id
  sectorName: { type: String }, // 部门名称
  allStaff: { type: Array }, // 部门所有员工
  staffNum: { type: Number }, // 部门员工数量
  addTime: { type: Number }, // 添加部门时间
  manage: { type: String }, // 部门经理
});
module.exports = mongoose.model("sectors", sectorSchema);
