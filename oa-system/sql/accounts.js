const mongoose = require("./db");
const accountSchema = new mongoose.Schema({
  accountId: { type: String }, // 账号id
  staffId: { type: String }, // 员工id
  sectorId: { type: String }, // 部门id
  username: { type: String }, // 用户名
  password: { type: String }, // 密码
  state: 0, // 状态 0启用 1禁用
  avatar: { type: String }, // 头像
  tel: { type: String }, // 手机号
  role: 0, // 权限 管理员0 经理1 员工2
});
module.exports = mongoose.model("accounts", accountSchema);
//* token 的数据
//* staffId: data[0].staffId,
//* username: data[0].username,
//* sectorId: data[0].sectorId,
//* accountId: data[0].accountId,
//* role: data[0].role,