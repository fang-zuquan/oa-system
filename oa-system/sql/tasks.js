const mongoose = require("./db");
const taskSchema = new mongoose.Schema({
    tasksId: { type: String }, // 任务id
    title: { type: String }, // 任务 标题
    startTime: { type: String }, // 完成时间 开始
    endTime: { type: String }, // 完成时间 结束
    creatTime: { type: String }, // 创建任务时间
    content: { type: String }, // 任务详情
    sendName: { type: String },
    sendId: { type: String },
    staffId: { type: String }, // 员工Id
    staffName: { type: String }, //员工名
    approveTime: { type: String }, // 完成时间
    approveState: { type: String }, //状态
    postscript: { type: String }, //备注
});
module.exports = mongoose.model("tasks", taskSchema);
