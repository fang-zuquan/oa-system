const mongoose = require("./db");
const fundSchema = new mongoose.Schema({
  applyId: { type: String }, // 申请id
  applyType: { type: Number }, // 申请类型 0请假 1加班 2补卡 3出差 4离职 5招聘 6经费
  title: { type: String }, // 标题
  content: { type: String }, // 原因
  approve: { type: String }, // 批准人的员工id
  approvePeople: { type: String },
  approveTime: { type: Number }, // 审批时间
  approveState: { type: Number }, // 审批状态 0审批中 1已通过 2已拒绝
  approveRemark: { type: String }, // 备注
  staffId: { type: String }, // 发起人的员工id
  sendPeople: { type: String },
  startTime: { type: Number }, // 开始时间
  endTime: { type: Number }, // 结束时间
  singleTime: { type: Number }, // 单时间类申请
  money: { type: Number }, // 经费金额
  payEndTime: { type: Number }, // 付款期限
  sectorId: { type: String },// 招聘部门的id
  job: { type: String }, // 申请岗位
});
module.exports = mongoose.model("apply", fundSchema);
