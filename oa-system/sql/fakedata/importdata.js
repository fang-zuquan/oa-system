const accounts = require("../accounts");
const sectors = require("../sectors");
const staffs = require("../staffs");
const { log } = console;
const fs = require("fs");
accounts.deleteMany({}, (err) => {});
sectors.deleteMany({}, (err) => {});
staffs.deleteMany({}, (err) => {});
// 账号插入
fs.readFile("./accounts.json", "utf-8", (err, data) => {
    if (err) {
        log("文件读取失败!" + err.message);
    } else {
        log(JSON.parse(data));
        accounts.insertMany(JSON.parse(data), (err, data) => {
            if (!err) {
                log("账号插入成功");
            } else {
                log(err);
            }
        });
        log("accounts.json 文件读取成功");
    }
});
// 部门插入
fs.readFile("./sectors.json", "utf-8", (err, data) => {
    if (err) {
        log("文件读取失败!" + err.message);
    } else {
        log("sectors.json 文件读取成功");
        sectors.insertMany(JSON.parse(data), (err, data) => {
            if (!err) {
                log("部门插入成功");
            } else {
                log(err);
            }
        });
    }
});
// 员工插入
fs.readFile("./staffs.json", "utf-8", (err, data) => {
    if (err) {
        log("文件读取失败!" + err.message);
    } else {
        log("staffs.json 文件读取成功");
        staffs.insertMany(JSON.parse(data), (err, data) => {
            if (!err) {
                log("员工插入成功");
            } else {
                log(err);
            }
        });
    }
});
