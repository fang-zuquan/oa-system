const mongoose = require("./db");
const logsSchema = new mongoose.Schema({
  id: { type: String },           // 日志id
  username: { type: String },     // 用户
  accountId: { type: String },     // 用户Id
  operate: { type: String },      // 操作行为
  operateTime: { type: Number },  // 操作时间
  ipAddress: { type: String },    // IP地址
});
module.exports = mongoose.model("logs", logsSchema);
