const mongoose = require("./db");
const clockinginSchema = new mongoose.Schema({
  id: { type: String }, // 员工id
  name: { type: String }, // 员工姓名
  checkData: { type: Array }, // 打卡数据
  // [{morning: 早晨打卡时间, afternoon: 下午打卡时间, status:本次考勤的状态},{...}]
});
module.exports = mongoose.model("clockingin", clockinginSchema);
