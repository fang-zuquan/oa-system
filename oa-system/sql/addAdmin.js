const accounts = require("../sql/accounts");
const staffs = require("../sql/staffs");
const sectors = require("../sql/sectors");
const md5 = require("md5");
const uuid = require("uuid");
const staffId = "staff-" + uuid.v1();
const sectorId = "sector-" + uuid.v1();
const accountsMsg = {
  accountId: "account-" + uuid.v1(),
  staffId, // 员工id
  sectorId,
  username: "admin", // 用户名
  password: md5("123456"), // 密码
  state: 0, // 状态 0启用 1禁用
  avatar: "", // 头像
  tel: "12345678910",
  role: 0, // 权限 管理员0 经理1 员工2
};
const staffMsg = {
  staffId, // 员工编号 staff-111111
  sectorId, // 部门id
  staffName: "超级管理员", // 姓名
  sex: null, // 性别 男0 女1
  address: "", // 地址
  nation: "", // 民族
  mianmao: null, // 政治面貌
  tel: "12345678910", // 手机号
  cardId: "", // 身份证号
  school: "", // 毕业学校
  graDate: null, // 毕业日期
  major: "", // 专业
  eduBg: null, // 学历 中专0 大专1 本科2 研究生3
  degree: null, // 学位 无0 学士1 硕士2 博士3
  job: "管理员", // 职位
  remarks: "", // 备注
  salary: 99999, // 薪资
  staffType: 0, // 员工类型 "正式员工0、合同工1、临时工2"
  workStatus: 0, // 在职状态 "在职0、离职1、退休2"
  joinTime: 0, // 入职时间
  leaveTime: -1, // 离职时间
};
const sectorMsg = {
  sectorId,
  sectorName: "行政部-管理员办公室",
  allStaff: [staffId],
  staffNum: 1,
  addTime: 0,
  manage: staffId,
};
module.exports = () => {
  accounts.find({ username: "admin" }, (err, data) => {
    if (!err) {
      if (data.length < 1) {
        accounts.insertMany(accountsMsg, err => {
          if (!err) console.log("管理员账户添加成功");
        });
        staffs.insertMany(staffMsg, err => {
          if (!err) console.log("管理员员工信息添加成功");
        });
        sectors.insertMany(sectorMsg, err => {
          if (!err) console.log("管理员部门信息添加成功");
        });
      } else {
        console.log("超管信息：admin，123456");
      }
    } else {
      console.log("插入管理员失败");
    }
  });
};
