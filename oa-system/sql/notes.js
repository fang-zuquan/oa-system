const mongoose = require("./db");
const noteSchema = new mongoose.Schema({
  noteid: { type: String }, // 公告id
  title: { type: String }, // 公告标题
  content: { type: String }, // 公告内容
  sendTime: { type: Number }, // 发布时间
  accountId: { type: String }, // 发布人的id
  staffName: { type: String }, //发布人姓名
  job: { type: String }, //职位
});
module.exports = mongoose.model("notes", noteSchema);
