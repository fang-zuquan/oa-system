const mongoose = require("./db");
const salarysetSchema = new mongoose.Schema({
  sendDate: { type: Number }, // 工资发放日期 每月？号
  lateFine: { type: Number }, // 迟到扣款 ？元/次
  earlyFine: { type: Number }, // 早退扣款 ？元/次
  absenceFine: { type: Number }, // 旷工扣款 ？元/次
  lackFine: { type: Number }, // 缺卡扣款 ？元/次
  overWork: { type: Number }, // 加班奖励 ?元/小时
  addTime: { type: Number }, // 添加时间
});
module.exports = mongoose.model("salaryset", salarysetSchema);
