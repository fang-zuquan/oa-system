const mongoose = require("./db");
const contractSchema = new mongoose.Schema({
  contractId: { type: String }, // 合同编号
  contractName: { type: String }, // 合同名称
  id: { type: String },//员工编号
  employeeName: { type: String },//员工姓名
  // contractType: { type: Number }, // 合同类型 劳务合同0 保密合同1 其他合同2
  contractType: { type: String }, // 合同类型 劳务合同 保密合同 其他
  effectiveDate: { type: Number }, // 合同生效日期
  endDate: { type: Number }, // 合同结束日期
  addedDate: { type: Number }, // 合同(首次)添加日期
  updatedDate: { type: Number }, // 合同（最近）更新日期
  notes: { type: String }, // 备注
});
module.exports = mongoose.model("contract", contractSchema);
