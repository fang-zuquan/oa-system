const express = require("express");
const router = express.Router();
const {
    login,
    addAccount,
    editAccount,
    getAccounts,
    modAccount,
    addStaff,
    modStaff,
    getStaff,
    delStaff,
    getSectors,
    addSector,
    editSector,
    delSector,
    setSalary,
    getSectorStaff,
    delnoter,
    getnoter,
    addnoter,
    editnoter,
    addTasks,
    getTasks,
    editTasks,
    getapply,
    editapply,
    addapply,
    getLogs,
    addContract,
    getContract,
    modContract,
    getClockingin,
    getName,
    avatar,
} = require("./api");
router.get("/login", login);
router.post("/addaccount", addAccount);
router.get("/getaccounts", getAccounts);
router.post("/editaccount", editAccount);
router.post("/modaccount", modAccount);
router.post("/avatar", avatar);
router.post("/addstaff", addStaff);
router.post("/modstaff", modStaff);
router.get("/getstaff", getStaff);
router.post("/delstaff", delStaff);
router.get("/getsectors", getSectors);
router.post("/addsector", addSector);
router.post("/editsector", editSector);
router.post("/delsector", delSector);
router.post("/setsalary", setSalary);
router.post("/getlogs", getLogs);
router.post("/getsectorstaff", getSectorStaff);
router.post("/addtasks", addTasks);
router.get("/gettasks", getTasks);
router.post("/edittasks", editTasks);
router.post("/delnoter", delnoter);
router.post("/getnoter", getnoter);
router.post("/addnoter", addnoter);
router.post("/editnoter", editnoter);
router.post("/getapply", getapply);
router.post("/editapply", editapply);
router.post("/addapply", addapply);
router.post("/addcontract", addContract);
router.get("/getcontract", getContract);
router.post("/modcontract", modContract);
router.get("/getclockingin", getClockingin);
router.get("/getname", getName);
module.exports = router;
