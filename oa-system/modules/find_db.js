module.exports = async (dbName, query, req, config = { sort: false }) => {
    try {
        const { sort, sortReq } = config;
        const data = await dbName
            .find(query, { _id: 0, __v: 0 })
            .sort(sort ? { [sortReq]: -1 } : void 0);
        return data;
    } catch (error) {
        console.log(error);
        req.send({
            code: 500,
            msg: "数据查询出错",
        });
    }
};
