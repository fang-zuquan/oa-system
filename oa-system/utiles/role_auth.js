const mustManageList = [
    // "/login",
    "/addAccount",
    "/editAccount",
    // "/getAccounts",
    // "/modAccount",
    "/addStaff",
    // "/modStaff",
    // "/getStaff",
    "/delStaff",
    // "/getSectors",
    "/addSector",
    "/editSector",
    "/delSector",
    "/setSalary",
    // "/getSectorStaff",
    // "/delnoter",
    // "/getnoter",
    // "/addnoter",
    // "/editnoter",
    // "/addTasks",
    // "/getTasks",
    // "/editTasks",
    // "/getapply",
    // "/editapply",
    // "/addapply",
    // "/getLogs",
    // "/addContract",
    // "/getContract",
    // "/modContract",
    // "/getClockingin",
    // "/getName",
    // "/avatar",
];

module.exports = function (req, res, next) {
    const { tokenData: { role } = {}, path } = req;
    if (mustManageList.includes(path) && role !== 0) {
        res.send({
            code: 403,
            msg: "权限不够，请联系管理员",
        });
        return;
    }
    next();
};
