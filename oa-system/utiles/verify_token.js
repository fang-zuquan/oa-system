const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
    if (req.path === "/login") {
        next();
        return;
    }
    const token = req?.query?.token || req?.body?.token;
    if (!token) {
        return res.send({
            code: 403,
            msg: "必须传递 token",
        });
    }
    try {
        req.tokenData = jwt.verify(token, "有志者事竟成");
        next();
    } catch (err) {
        console.log("token middleware", "err");
        return res.send({
            code: 401,
            msg: "token 失效",
        });
    }
};
