const logs = require("../sql/logs");
const uuid = require("uuid");

const operate_map = {
    "/login": "用户登录",
    "/addaccount": "添加账号",
    "/editaccount": "编辑账号",
    // "/getaccounts": "查询账号",
    "/modaccount": "修改账号",
    "/addstaff": "添加员工",
    "/modstaff": "修改员工信息",
    // "/getstaff": "获取员工",
    "/delstaff": "删除员工",
    "/getsectors": "获取部门",
    "/addsector": "添加部门",
    "/editsector": "编辑部门",
    "/delsector": "删除部门",
    "/setsalary": "设置薪水",
    // "/getsectorstaff": "获取部门员工",
    "/delnoter": "删除公告",
    // "/getnoter": "获取公告",
    "/addnoter": "添加公告",
    "/editnoter": "编辑公告",
    "/addtasks": "添加任务",
    // "/gettasks": "获取任务",
    "/edittasks": "编辑任务",
    // "/getapply": "获取申请",
    "/editapply": "编辑申请",
    "/addapply": "添加申请",
    // "/getlogs": "获取日志",
    "/addcontract": "添加合同",
    // "/getcontract": "获取合同",
    "/modcontract": "修改合同",
    // "/getclockingin": "获取打卡信息",
    // "/getname": "获取员工",
    "/avatar": "编辑头像",
};

function getClientIp(req) {
    let ipAddress;
    let forwardedIpsStr = req.headers["X-Forwarded-For"]; //判断是否有反向代理头信息
    if (forwardedIpsStr) {
        //如果有，则将头信息中第一个地址拿出，该地址就是真实的客户端IP；
        let forwardedIps = forwardedIpsStr.split(",");
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        //如果没有直接获取IP；
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
}

module.exports = function (req, res, next) {
    try {
        const { tokenData, path } = req;
        if (operate_map[path]) {
            logs.insertMany({
                id: "log-" + uuid.v1(),
                username: tokenData?.username || req?.qurey?.username,
                accountId: tokenData?.accountId,
                operate: operate_map[path],
                operateTime: Date.now(),
                ipAddress: getClientIp(req),
            });
        }
        next();
    } catch (error) {
        console.log("log", error);
    }
};
