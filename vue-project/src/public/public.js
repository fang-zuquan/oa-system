import { ElMessage } from 'element-plus'
//项目运行环境  0：开发环境  1：生产环境
const runMode = 0;    //项目成员非必要尽量不要修改 / 如项目成员需要在不同环境测试，请在提交代码前记得改回 / 上线前统一修改成runMode = 0 /
//本地测试接口 - 开发环境使用
const mockBaseURL = 'http://localhost:3000';
//项目上线接口 - 生产环境使用
const real_url_select = 1;  // 项目成员可以修改（如果项目成员测试发现接口不通，改成其它（下面url地址不要随意修改），可以改成1、2、3）
const real_url_one = 'http://ynight.tk:3003/';
const real_url_two = 'http://169.254.150.5:5173/';
const real_url_three = 'http://192.168.2.1:5173/';
const realBaseURL = (real_url_select === 1) ? real_url_one : ((real_url_select === 2) ? real_url_two : real_url_three);
export const REQUEST_BASE_URL = (runMode === 0) ? mockBaseURL : realBaseURL;
//角色所有数据
export const roles = [{
    value: 0,
    label: '管理员'
}, {
    value: 1,
    label: '经理'
}, {
    value: 2,
    label: '员工'
}]
// text   // 权限  0 -> 管理员  1 -> 经理  2 -> 员工
export function roleToName(num) {
    return (num === 0) ? '管理员' : (num === 1) ? '经理' : '员工';
}
// export function nameToRole(name) {
//     return (name === '管理员') ? 0 : (name === '经理') ? 1 : 2;
// }
// state  // 状态 0 -> 启用   1 -> 禁用
export function stateToName(num) {
    return (num === 0) ? '启用' : '禁用';
}
// text  // 状态 启用 -> 0   禁用 -> 1
export function nameToState(name) {
    return (name === '启用') ? 0 : 1;
}
// 再次简单封装，消息提示功能
export function showMessage(message, type, duration) {
    return ElMessage({
        showClose: true,
        message,
        type,
        duration
    });
}
//封装： 请求网络错误 - 文案提示
export function showNetError(res) {
    if (res.code === 'ERR_NETWORK') {
        const error_msg = "<p>网络异常哦，<br></br> 请检查网络，或者稍后重试</p>";
        return ElMessage({
            showClose: true,
            dangerouslyUseHTMLString: true,
            center: true,
            message: error_msg,
            type: 'error',
            duration: 3000
        })
    }
}
//封装：时间戳转换日期模式
export function timestampToTime(timestamp) {
    if (timestamp <= 0) return 0;
    // 时间戳为10位
    let date = new Date(timestamp);
    let Y = date.getFullYear() + "-";
    let M =
        (date.getMonth() + 1 < 10
            ? "0" + (date.getMonth() + 1)
            : date.getMonth() + 1) + "-";
    let D = (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + " ";
    let h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ":";
    let m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ":";
    let s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return Y + M + D + h + m + s;
}
//字符串转数字（这里转float型数字）
export function strToNum(str) {
    return parseFloat(str);
}