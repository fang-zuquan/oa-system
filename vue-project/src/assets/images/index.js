import icon_1 from "./header_icon_01.png";
import icon_2 from "./header_icon_02.png";
import icon_3 from "./header_icon_03.png";
import icon_4 from "./header_icon_04.png";
import icon_5 from "./header_icon_05.png";
import icon_6 from "./header_icon_06.png";
import icon_7 from "./header_icon_07.png";
import icon_8 from "./header_icon_08.png";

export default [
    { title: "请假申请", src: icon_1 },
    { title: "加班申请", src: icon_2 },
    { title: "补卡申请", src: icon_3 },
    { title: "出差申请", src: icon_4 },
    { title: "离职申请", src: icon_5 },
    { title: "招聘申请", src: icon_6 },
    { title: "经费预算申请", src: icon_7 },
    { title: "我的任务", src: icon_8 },
];
