import { ref } from "vue";
import { defineStore } from "pinia";
export const useUserStore = defineStore("user", () => {
    let userMsgLocal = localStorage.getItem("userMsg");
    userMsgLocal = userMsgLocal ? JSON.parse(userMsgLocal) : {};
    const userMsg = ref(userMsgLocal);
    function change_userMsg(obj) {
        if (obj === null) {
            userMsg.value = {};
        } else {
            obj.token && (userMsg.value.token = obj.token);
            obj.username && (userMsg.value.username = obj.username);
            obj.staffId && (userMsg.value.staffId = obj.staffId);
            obj.role && (userMsg.value.role = obj.role);
            obj.accountId && (userMsg.value.accountId = obj.accountId);
            obj.avatar && (userMsg.value.avatar = obj.avatar);
        }
    }
    return { userMsg, change_userMsg };
});
export const useSectorsStore = defineStore("sectors", () => {
    let sectorsMsgLocal = localStorage.getItem("sectorsMsg");
    sectorsMsgLocal = sectorsMsgLocal ? JSON.parse(sectorsMsgLocal) : {};
    const sectorsMsg = ref(sectorsMsgLocal);
    function change_sectorsMsg(obj) {
        // if(obj === null){
        //   sectorsMsg.value = {};
        // }else{
        //   obj.sectorId && (sectorsMsg.value.sectorId = obj.sectorId);
        //   obj.sectorName && (sectorsMsg.value.sectorName = obj.sectorName);
        // }
        sectorsMsg.value = obj;
    }
    return { sectorsMsg, change_sectorsMsg };
});
// // 任务冲存储
// export const useNoteStore = defineStore('user', () => {
//   // 为了持久化存储状态，借助本地存储
//   let userMsgLocal = localStorage.getItem("userMsg");
//   userMsgLocal = userMsgLocal ? JSON.parse(userMsgLocal) : {};
//   // 创建响应式状态
//   const userMsg = ref( userMsgLocal );
//   // userMsg对象可能包含的字段：{token, uName, uId, power, avatar}
//   function change_userMsg(obj){
//     if(obj === null){
//       userMsg.value = {};
//     }else{
//     }
//   }
//   // 暴露响应式状态和方法
//   return { userMsg, change_userMsg }
// })
