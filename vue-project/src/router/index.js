import { createRouter, createWebHistory } from "vue-router";
import HomeView from "@/views/HomeView.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/mytask",
      name: "mytask",
      component: () => import("@/views/personalOffice/MyTaskView.vue"),
    },
    // {
    //   path: '/creattask',
    //   name: 'creattask',
    //   component: () => import('@/views/components/CreatTask.vue')
    // },
    {
      path: "/apply/:id",
      name: "apply",
      props: true,
      component: () => import("@/views/personalOffice/ApplyView.vue"),
    },
    {
      path: "/staff",
      name: "staff",
      component: () => import("@/views/human/StaffView.vue"),
    },
    {
      path: "/editstaff/:type/:id",
      name: "editstaff",
      props: true,
      component: () => import("@/views/human/EditStaff.vue"),
    },
    // {
    //   path: "/editstaff",
    //   name: "editstaff",
    //   beforeEnter: (to, from) => {
    //     console.log("路由from对象", from);
    //     console.log("路由to对象", to);
    //   },
    //   component: () => import("@/views/human/EditStaff.vue"),
    // },
    {
      path: "/contract",
      name: "contract",
      component: () => import("@/views/human/ContractView.vue"),
    },
    {
      path: "/sector",
      name: "sector",
      component: () => import("@/views/human/SectorView.vue"),
    },
    {
      path: "/salarymanage",
      name: "salarymanage",
      component: () => import("@/views/salary/SalaryManageView.vue"),
    },
    {
      path: "/salaryset",
      name: "salaryset",
      component: () => import("@/views/salary/SalarySetView.vue"),
    },
    {
      path: "/attend",
      name: "attend",
      component: () => import("@/views/attend/AttendView.vue"),
    },
    {
      path: "/system",
      name: "system",
      component: () => import("@/views/system/SystemView.vue"),
    },
    {
      path: "/accountmanage",
      name: "accountmanage",
      component: () => import("@/views/accountManage/AccountManageView.vue"),
    },
    {
      path: "/accountsetting",
      name: "accountsetting",
      component: () => import("@/views/accountManage/AccountSetting.vue"),
    },
    // 添加
    {
      path: '/addnotice',
      name: 'addnotice',
      component: () => import('../views/system/addNotice.vue')
    },
    {
      path: "/editaccount",
      name: "editaccount",
      component: () => import("@/views/accountManage/EditAccountView.vue"),
    },
    {
      path: "/logger",
      name: "logger",
      component: () => import("@/views/logger/LoggerView.vue"),
    },
  ],
});
export default router;
