import axios from "../tools/request";
function getTasks(token, tasksId) {
    const obj = { token };
    tasksId && (obj.tasksId = tasksId);
    return axios.get("/gettasks", {
        params: obj,
    });
}
function editTasks(obj) {
    return axios.post("/edittasks", obj);
}
function addTasks(obj) {
    return axios.post("/addtasks", obj);
}

export default {
    getTasks,
    editTasks,
    addTasks,
};
