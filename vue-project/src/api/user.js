import axios from "../tools/request";
function loginApi(un, pw) {
    return axios.get("/login", {
        params: {
            username: un,
            password: pw,
        },
    });
}
function getAccounts(token, accountId) {
    return axios.get("/getaccounts", {
        params: {
            token,
            accountId,
        },
    });
}
function addAccount(obj) {
    return axios.post("/addaccount", obj);
}
function modAccount(obj) {
    return axios.post("/modaccount", obj);
}
function editAccount(obj) {
    return axios.post("/editaccount", obj);
}

export default {
    loginApi,
    getAccounts,
    addAccount,
    modAccount,
    editAccount,
};
