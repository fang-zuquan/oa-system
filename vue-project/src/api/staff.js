import axios from "@/tools/request";
// 获取员工
function getStaff(ops) {
    return axios.get("/getstaff", {
        params: ops,
    });
}
// 添加员工
function addStaff(ops) {
    return axios.post("/addstaff", ops);
}
// 删除员工
function delStaff(token, staffId) {
    return axios.post("/delstaff", { token, staffId });
}
// 修改员工
function modStaff(ops) {
    return axios.post("/modstaff", ops);
}

export default {
    getStaff,
    addStaff,
    delStaff,
    modStaff,
};
