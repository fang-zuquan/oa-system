import axios from "../tools/request";

function getApply(ops) {
    return axios.post("/getapply", ops);
}
function addApply(obj) {
    return axios.post("/addapply", obj);
}
function editApply(obj) {
    return axios.post("/editapply", obj);
}

export default {
    getApply,
    addApply,
    editApply,
};
