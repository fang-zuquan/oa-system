import axios from "../tools/request";
export default {
    getLoggerApi(obj) {
        return axios.post("/getlogs", obj);
    },
};
