import axios from "../tools/request";
function getNote({ token, noteId, noteTitle }) {
    return axios.post("/getnoter", {
        token,
        noteId,
        noteTitle,
    });
}
function delNote(ops) {
    return axios.post("/delnoter", ops);
}
function etidNote(ops) {
    return axios.post("/editnoter", ops);
}
function addNote(ops) {
    return axios.post("/addnoter", ops);
}

export default {
    getNote,
    delNote,
    etidNote,
    addNote,
};
