import axios from "../tools/request";
// 添加合同
function addContract(ops) {
    return axios.post("/addcontract", ops);
}
function getContract(ops) {
    return axios.get("/getcontract", { params: ops });
}
function modContract(ops) {
    return axios.post("/modcontract", ops);
}
function getName(ops) {
    return axios.get("/getName", {
        params: ops,
    });
}

export default {
    addContract,
    getContract,
    modContract,
    getName,
};
