import user from "./user";
import tasks from "./tasks";
import apply from "./apply";
import contract from "./contract";
import note from "./note";
import sectors from "./sectors";
import staff from "./staff";
import clockingin from "./clockingin";
import logger from "./logger";
import salary from "./salary";

export const {
    getsectorsApi,
    addsectorApi,
    editsectorApi,
    delsectorApi,
    getsectorstaffApi,
} = sectors;

export const { loginApi, getAccounts, addAccount, modAccount, editAccount } =
    user;

export const { getTasks, editTasks, addTasks } = tasks;
export const { getApply, addApply, editApply } = apply;
export const { addContract, getContract, modContract, getName } = contract;
export const { getNote, delNote, etidNote, addNote } = note;
export const { getStaff, addStaff, delStaff, modStaff } = staff;
export const { getClockingin } = clockingin;
export const { getLoggerApi } = logger;
export const { setSalaryApi } = salary;
