import axios from "../tools/request";
// 暴露 请求 薪酬设置功能
export default {
    setSalaryApi(obj) {
        return axios.post("/setsalary", obj);
    },
};
