import axios from "../tools/request";

function getsectorsApi(token) {
    return axios.get("/getsectors", { params: { token } });
}
function addsectorApi(obj) {
    return axios.post("/addsector", obj);
}
function editsectorApi(obj) {
    return axios.post("/editsector", obj);
}
function delsectorApi(obj) {
    return axios.post("/delsector", obj);
}
function getsectorstaffApi(obj) {
    return axios.post("/getsectorstaff", obj);
}

export default {
    getsectorsApi,
    addsectorApi,
    editsectorApi,
    delsectorApi,
    getsectorstaffApi,
};
