import axios from "../tools/request";
export default {
    getClockingin(ops) {
        return axios.get("/getclockingin", { params: ops });
    },
};
